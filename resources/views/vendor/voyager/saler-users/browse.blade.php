@extends('flotas.layout.master')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div id="btnAccion">
                        <a href="{{ route('new.saler_users') }}" class="btn btn-warning btn-fill btn-wd">Agregar relacion</a>
                    </div>
                    <br>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            {{--<div class="pull-right">--}}
                                {{--<a  href="{{ route('flotas.dashboard.descargar.ordenes') }}" class="btn btn-info btn-fill btn-wd">--}}
                                    {{--Descargar--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            <h4 class="title">Lista de relaciones</h4>
                            <p class="category">Here is a subtitle for this table</p>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <?php $sale_users = App\SalerUser::all(); ?>
                            @if(count($sale_users)>0)
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Relación</th>
                                            <th>Jefe de Venta</th>
                                            <th>Vendedor</th>
                                            <th>Fecha de Creacion</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sale_users as $sale_user)
                                        <tr>
                                            <td>{{ $sale_user->id }}</td>
                                            <td>{{ App\User::find($sale_user->saler_id)->name }}</td>
                                            <td>{{ App\User::find($sale_user->user_id)->name }}</td>
                                            <td>{{ $sale_user->created_at }}</td>
                                            <td style="text-align: center"><a href="{{ route('delete.saler_users', encrypt($sale_user->id)) }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h4>No hay relaciones</h4>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


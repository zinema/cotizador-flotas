@extends('flotas.layout.master')

@section('content')
    <div class="row">
        <form action="{{route('store.saler_users')}}" method="POST">
            {{ csrf_field() }}
            <div class="col-md-12">
                <div class="form-group>
                        <label for=" name
                    " >Jefe de Ventas</label>
                    <select class="form-control" name="saler" id="saler">
                        <option value disabled>Elige un jefe de ventas</option>
                        @foreach(App\User::where('role_id', 3)->get() as $saler)
                            <option value="{{$saler->id}}">{{ $saler->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group>
                            <label for=" name
                    " >Vendedor</label>
                    <select class="form-control" name="user" id="user">
                        <option value disabled>Elife un vendedor</option>
                        @foreach(App\User::where('role_id', 2)->get() as $user)
                            <option value="{{$user->id}}">{{$user->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <hr>
            <div class="col-md-12">
                <div class="pull-right">
                    <button type="submit" class="btn btn-success btn-fill btn-wd">Agregar</button>
                </div>
            </div>
        </form>
    </div>

@stop


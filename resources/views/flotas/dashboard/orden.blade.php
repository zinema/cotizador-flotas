@extends('flotas.layout.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <?php $user = Auth::user(); ?>
            <div id="main">
                <ul class="nav nav-tabs">
                    <li id="li-vendedor" class="active"><a data-toggle="tab" href="#vendedor">Vendedor</a></li>
                    <li id="li-empresa"><a data-toggle="tab" href="#empresa">Empresa</a></li>
                    <li id="li-cliente"><a data-toggle="tab" href="#cliente">Contacto</a></li>
                    <li id="li-vehiculos"><a data-toggle="tab" href="#vehiculos">Vehiculo</a></li>
                    <li id="li-adicional"><a data-toggle="tab" href="#adicional">Adicional</a></li>
                    <!-- <li><a data-toggle="tab" href="#exportar">Exportar</a></li> -->
                </ul>
                <div class="tab-content">
                    <div id="vendedor" class="tab-pane active">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombres">Nombres</label>
                                    <input type="text" class="form-control" id="nombres" name="nombres"
                                           value="{{ $user->name }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="apellidos">Apellidos</label>
                                    <input type="text" class="form-control" id="apellidos" name="apellidos"
                                           value="{{ $user->apellido }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email"
                                           value="{{ $user->email }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Telefono</label>
                                    <input type="text" class="form-control" id="telefono" name="telefono"
                                           value="{{ $user->telefono }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success" id="continuar_vendedor">Continuar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="empresa" class="tab-pane">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="razon_social_empresa">Razón Social</label>
                                    <input type="text" class="form-control" id="razon_social_empresa">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ruc_empresa">RUC</label>
                                    <input type="text" class="form-control" id="ruc_empresa">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono_empresa">Telefono</label>
                                    <input type="text" class="form-control" id="telefono_empresa">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="direccion_empresa">Dirección</label>
                                    <input type="text" class="form-control" id="direccion_empresa">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success" id="continuar_empresa">Continuar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="cliente" class="tab-pane">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombres_cliente">Nombres</label>
                                    <input type="text" class="form-control" id="nombres_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="apellidos_cliente">Apellidos</label>
                                    <input type="text" class="form-control" id="apellidos_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dni_cliente">Dni</label>
                                    <input type="email" class="form-control" id="dni_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="celular_cliente">Celular</label>
                                    <input type="text" class="form-control" id="celular_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email_cliente">Email</label>
                                    <input type="text" class="form-control" id="email_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono">Fuente de contacto</label>
                                    <input type="text" class="form-control" id="fuente_contacto_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fecha_compra_cliente">Fecha estimada de compra</label>
                                    <input type="text" class="form-control" id="fecha_compra_cliente">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success" id="continuar_cliente">Continuar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="vehiculos" class="tab-pane">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Modelos</label>
                                    <select id="modelo_1" class="form-control">
                                        <option disabled>Seleccionar Modelo</option>
                                        @foreach($modelos as $modelo)
                                            <option value="{{ $modelo->id }}">{{ $modelo->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div id="div-version_1"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Año modelo</label>
                                    <select id="year_modelo_1" class="form-control">
                                        <option disabled>Seleccionar Año Modelo</option>
                                        <option value="1">2014 ARN9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cantidad">Cantidad</label>
                                    <input type="text" class="form-control" id="cantidad_1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fabricacion">Fabricación</label>
                                    <input type="text" class="form-control" id="fabricacion_1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="combustible">Combustible</label>
                                    <input type="text" class="form-control" id="combustible_1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <input type="text" class="form-control" id="color_1">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Modelos</label>
                                    <select id="modelo_2" class="form-control">
                                        <option disabled>Seleccionar Modelo</option>
                                        @foreach($modelos as $modelo)
                                            <option value="{{ $modelo->id }}">{{ $modelo->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div id="div-version_2"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Año modelo</label>
                                    <select id="year_modelo_2" class="form-control">
                                        <option disabled>Seleccionar Año Modelo</option>
                                        <option value="1">2014 ARN9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cantidad">Cantidad</label>
                                    <input type="text" class="form-control" id="cantidad_2">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fabricacion">Fabricación</label>
                                    <input type="text" class="form-control" id="fabricacion_2">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="combustible">Combustible</label>
                                    <input type="text" class="form-control" id="combustible_2">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <input type="text" class="form-control" id="color_2">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Modelos</label>
                                    <select id="modelo_3" class="form-control">
                                        <option disabled>Seleccionar Modelo</option>
                                        @foreach($modelos as $modelo)
                                            <option value="{{ $modelo->id }}">{{ $modelo->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div id="div-version_3"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Año modelo</label>
                                    <select id="year_modelo_3" class="form-control">
                                        <option disabled>Seleccionar Año Modelo</option>
                                        <option value="1">2014 ARN9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cantidad">Cantidad</label>
                                    <input type="text" class="form-control" id="cantidad_3">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fabricacion">Fabricación</label>
                                    <input type="text" class="form-control" id="fabricacion_3">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="combustible">Combustible</label>
                                    <input type="text" class="form-control" id="combustible_3">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <input type="text" class="form-control" id="color_3">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Modelos</label>
                                    <select id="modelo_4" class="form-control">
                                        <option disabled>Seleccionar Modelo</option>
                                        @foreach($modelos as $modelo)
                                            <option value="{{ $modelo->id }}">{{ $modelo->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div id="div-version_4"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelo">Año modelo</label>
                                    <select id="year_modelo_4" class="form-control">
                                        <option disabled>Seleccionar Año Modelo</option>
                                        <option value="1">2014 ARN9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cantidad">Cantidad</label>
                                    <input type="text" class="form-control" id="cantidad_4">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fabricacion">Fabricación</label>
                                    <input type="text" class="form-control" id="fabricacion_4">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="combustible">Combustible</label>
                                    <input type="text" class="form-control" id="combustible_4">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <input type="text" class="form-control" id="color_4">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success" id="continuar_vehiculo">
                                            Continuar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="adicional" class="tab-pane">
                        <div class="row">
                            <div class="row grupo_campos">
                                <div class="row campos_adicional">
                                    <div class="col-md-5">
                                        <label for="fabricacion">Nombre</label>
                                        <input type="text" class="form-control" id="nombre_adicional"
                                               name="adicional_nombres[]">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="fabricacion">Precio</label>
                                        <input type="text" class="form-control" id="precio_adicional"
                                               name="adicional_precios[]">
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-simple btn-wd eliminar_campo">Eliminar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin: 0 !important;">
                                <div class="text-right">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-info agregar_campo">Agregar Campo &nbsp; <span
                                                style="font-size:16px; font-weight:bold;">+ </span></button>
                                </div>
                            </div>
                            <div class="row" style="margin: 0 !important;">
                                <div class="text-right">
                                    <label>&nbsp;</label>
                                    <button type="submit" class="btn btn-success" id="continuar_adicional">Continuar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="exportar" class="tab-pane">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="GET" action="{{ route('flotas.download.pdf') }}">
                                    <input type="hidden" name="orden_id" id="orden_id">
                                    <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                                    <button type="submit" id="exportar" class="btn btn-info">Exportar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        var orden_id = '';

        $("#continuar_vendedor").click(function () {
            var token = "{{ csrf_token() }}";
            var nombres = $("#nombres").val();
            var apellidos = $("#apellidos").val();
            var email = $("#email").val();
            var telefono = $("#telefono").val();
            var user_id = $("#user_id").val();

            $.ajax({
                url: "{{ route('flotas.store.orden') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    nombres: nombres,
                    apellidos: apellidos,
                    email: email,
                    telefono: telefono
                },
                success: function (d) {
                    if (d) {
                        $("#orden_id").val(d);
                        $('#vendedor').removeClass('active');
                        $('#empresa').addClass('active');
                        $('#li-vendedor').removeClass('active');
                        $('#li-empresa').addClass('active');
                    } else {
                        swal("Error", "Completa los campos requeridos.", "error");
                    }
                }
            });
        });

        $("#continuar_empresa").click(function () {
            var token = "{{ csrf_token() }}";
            var razon_social_empresa = $("#razon_social_empresa").val();
            var ruc_empresa = $("#ruc_empresa").val();
            var telefono_empresa = $("#telefono_empresa").val();
            var direccion_empresa = $("#direccion_empresa").val();
            var orden_id = $("#orden_id").val();

            $.ajax({
                url: "{{ route('flotas.store.empresa') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    razon_social_empresa: razon_social_empresa,
                    ruc_empresa: ruc_empresa,
                    telefono_empresa: telefono_empresa,
                    direccion_empresa: direccion_empresa,
                    orden_id: orden_id
                },
                success: function (d) {
                    console.log(d);
                    $('#empresa').removeClass('active');
                    $('#cliente').addClass('active');
                    $('#li-empresa').removeClass('active');
                    $('#li-cliente').addClass('active');
                }
            });
        });

        $("#continuar_cliente").click(function () {
            var token = "{{ csrf_token() }}";
            var nombres_cliente = $("#nombres_cliente").val();
            var apellidos_cliente = $("#apellidos_cliente").val();
            var dni = $("#dni_cliente").val();
            var telefono = $("#celular_cliente").val();
            var email = $("#email_cliente").val();
            var fuente_contacto_cliente = $("#fuente_contacto_cliente").val();
            var fecha_compra_cliente = $("#fecha_compra_cliente").val();
            var orden_id = $("#orden_id").val();

            $.ajax({
                url: "{{ route('flotas.store.cliente') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    nombres_cliente: nombres_cliente,
                    apellidos_cliente: apellidos_cliente,
                    dni: dni,
                    telefono: telefono,
                    email: email,
                    fuente_contacto_cliente: fuente_contacto_cliente,
                    fecha_compra_cliente: fecha_compra_cliente,
                    orden_id: orden_id
                },
                success: function (d) {
                    $('#cliente').removeClass('active');
                    $('#vehiculos').addClass('active');
                    $('#li-cliente').removeClass('active');
                    $('#li-vehiculos').addClass('active');
                }
            });
        });

        @for ($i = 1; $i < 5; $i++)
        $("#modelo_{{$i}}").change(function () {
            var modelo_id_{{$i}} = $("#modelo_{{$i}}").val();
            var version = $("#div-version_{{$i}}");
            var select = '';
            $.ajax({
                url: "{{ route('flotas.listar.versiones') }}",
                type: 'GET',
                data: {modelo_id: modelo_id_{{$i}} },
                success: function (response) {
                    var versions = JSON.parse(response);
                    select += '<label for="version">Version</label>';
                    select += '<select name="version" class="form-control input-sm " required id="version_{{$i}}" >';
                    $.each(versions, function (index, version) {
                        select += '<option value=" ' + version.id + ' ">' + version.nombre + '</option>';
                    })
                    select += '</select>';

                    console.log(select);
                    version.html(select);
                }
            });
        });
        @endfor

        $("#continuar_vehiculo").click(function () {
            var token = "{{ csrf_token() }}";

            var modelo_1 = $("#modelo_1").val();
            var version_1 = $("#version_1").val();
            var cantidad_1 = $("#cantidad_1").val();
            var codigo_modelo_1 = $("#codigo_modelo_1").val();
            var fabricacion_1 = $("#fabricacion_1").val();
            var year_modelo_1 = $("#year_modelo_1").val();
            var combustible_1 = $("#combustible_1").val();
            var color_1 = $("#color_1").val();
            var modelo_2 = $("#modelo_2").val();
            var version_2 = $("#version_2").val();
            var cantidad_2 = $("#cantidad_2").val();
            var codigo_modelo_2 = $("#codigo_modelo_2").val();
            var fabricacion_2 = $("#fabricacion_2").val();
            var year_modelo_2 = $("#year_modelo_2").val();
            var combustible_2 = $("#combustible_2").val();
            var color_2 = $("#color_2").val();
            var modelo_3 = $("#modelo_3").val();
            var version_3 = $("#version_3").val();
            var cantidad_3 = $("#cantidad_3").val();
            var codigo_modelo_3 = $("#codigo_modelo_3").val();
            var fabricacion_3 = $("#fabricacion_3").val();
            var year_modelo_3 = $("#year_modelo_3").val();
            var combustible_3 = $("#combustible_3").val();
            var color_3 = $("#color_3").val();
            var modelo_4 = $("#modelo_4").val();
            var version_4 = $("#version_4").val();
            var cantidad_4 = $("#cantidad_4").val();
            var codigo_modelo_4 = $("#codigo_modelo_4").val();
            var fabricacion_4 = $("#fabricacion_4").val();
            var year_modelo_4 = $("#year_modelo_4").val();
            var combustible_4 = $("#combustible_4").val();
            var color_4 = $("#color_4").val();
            var orden_id = $("#orden_id").val();

            $.ajax({
                url: "{{ route('flotas.store.vehiculo') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    modelo_1: modelo_1,
                    version_1: version_1,
                    cantidad_1: cantidad_1,
                    codigo_modelo_1: codigo_modelo_1,
                    fabricacion_1: fabricacion_1,
                    year_modelo_1: year_modelo_1,
                    combustible_1: combustible_1,
                    color_1: color_1,
                    modelo_2: modelo_2,
                    version_2: version_2,
                    cantidad_2: cantidad_2,
                    codigo_modelo_2: codigo_modelo_2,
                    fabricacion_2: fabricacion_2,
                    year_modelo_2: year_modelo_2,
                    combustible_2: combustible_2,
                    color_2: color_2,
                    modelo_3: modelo_3,
                    version_3: version_3,
                    cantidad_3: cantidad_3,
                    codigo_modelo_3: codigo_modelo_3,
                    fabricacion_3: fabricacion_3,
                    year_modelo_3: year_modelo_3,
                    combustible_3: combustible_3,
                    color_3: color_3,
                    modelo_4: modelo_4,
                    version_4: version_4,
                    cantidad_4: cantidad_4,
                    codigo_modelo_4: codigo_modelo_4,
                    fabricacion_4: fabricacion_4,
                    year_modelo_4: year_modelo_4,
                    combustible_4: combustible_4,
                    color_4: color_4,
                    orden_id: orden_id
                },
                success: function (d) {
                    $('#vehiculos').removeClass('active');
                    $('#adicional').addClass('active');
                    $('#li-vehiculos').removeClass('active');
                    $('#li-adicional').addClass('active');
                }
            });
        });


        $("#continuar_adicional").click(function () {
            var adicional_nombre = [];
            var adicional_precio = [];
            var count_n = 0;
            var count_p = 0;
            var t = 0;
            $('input[name^="adicional_nombres"]').each(function () {
                adicional_nombre[count_n++] = $(this).val();
            });

            $('input[name^="adicional_precios"]').each(function () {
                adicional_precio[count_p++] = $(this).val();
            });

            var token = "{{ csrf_token() }}";
            var orden_id = $("#orden_id").val();

            $.ajax({
                url: "{{ route('flotas.store.adicional') }}",
                type: 'POST',
                data: {
                    _method: 'post',
                    _token: token,
                    adicional_nombre: adicional_nombre,
                    adicional_precio: adicional_precio,
                    orden_id: orden_id
                },
                success: function (d) {
                    $('#adicional').removeClass('active');
                    // $('#exportar').addClass('active');
                }
            });
        });
    </script>



    <script>
        $(document).ready(function () {
            var max_fields = 10;
            var wrapper = $(".grupo_campos");
            var add_button = $(".agregar_campo");

            var x = 1;
            $(add_button).click(function (e) {
                e.preventDefault();
                if (x < max_fields) {
                    x++;
                    var bloque = '<div class="row campos_adicional">' +
                        '<div class="col-md-5">' +
                        '<label for="fabricacion">Nombre</label>' +
                        '<input type="text" class="form-control" id="nombre_adicional" name="adicional_nombres[]">' +
                        '</div>' +
                        '<div class="col-md-5">' +
                        '<label for="fabricacion">Precio</label>' +
                        '<input type="text" class="form-control" id="precio_adicional" name="adicional_precios[]">' +
                        '</div>' +
                        '<div class="col-md-2">' +
                        '<button class="btn btn-simple btn-wd eliminar_campo">Eliminar</button>' +
                        '</div>' +
                        '</div>';
                    $(wrapper).append(bloque);
                }
                else {
                    alert('Llegaste al límite de inputs');
                }
            });

            $(wrapper).on("click", ".eliminar_campo", function (e) {
                e.preventDefault();
                $(this).parent('div').parent('.campos_adicional').remove();
                x--;
            })
        });
    </script>


@stop
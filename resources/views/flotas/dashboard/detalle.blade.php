@extends('flotas.layout.master')

@section('content')
    <?php $user = Auth::user(); ?>
    <input type="hidden" value="{{$user->id}}" id="user_id">
    <input type="hidden" value="{{$orden->id}}" id="orden_id">
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="header">
                    <label class="font-weight-bold">Detalle</label> <br>
                    #{{ $orden->id}}
                    <div class="pull-right">
                        <?php
                        $created = Carbon\Carbon::parse($orden->created_at);
                        ?>
                        {{ $created->format('l jS \\of F Y') }}
                    </div>
                </div>
                <div class="content">
                    <div class="panel-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <div>
                                    <label>Modelo</label><br>
                                    {{ $modelo->nombre }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div>
                                    <label>Version</label><br>
                                    {{ $version->nombre }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div>
                                    <label>Ano fabricación</label><br>
                                    {{ $orden->fabricacion }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div>
                                    <label>Ano modelo</label><br>
                                    {{ $orden->anio }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div>
                                    <label>Combustible</label><br>
                                    {{ $orden->combustible }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div>
                                    <label>Color</label><br>
                                    {{ $orden->color }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div>
                                    <label>Cantidad</label><br>
                                    {{ $orden->cantidad }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="card">
                <div class="header">
                    Cliente
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    Linea de Tiempo
                    <hr>
                </div>
                <div class="content">
                    <div class="form-group">
                        <textarea rows="5" class="form-control" id="comentario"
                                  placeholder="{{$user->name}} deja un comentario"></textarea>
                    </div>
                    <div class="form-group">
                        <button id="agregar" class="btn btn-warning btn-fill btn-wd">Agregar nota</button>
                    </div>

                    @foreach($comentarios as $comentario)
                        <div style="background-color: #cccccc; padding: 10px; margin:10px;border-radius: 5px;">
                            <label style="color: #2181a2">{{ $user->name }}</label><br>
                            <span>{{ $comentario->comentario }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $("#agregar").click(function(){
        var token = "{{ csrf_token() }}";
        var comentario = $("#comentario").val();
        var user_id = $("#user_id").val();
        var orden_id = $("#orden_id").val();

        $.ajax({
            url: "{{ route('flotas.dashboard.store.comentario') }}",
            type: 'POST',
            data: {_method: 'post', _token: token, comentario:comentario, user_id:user_id, orden_id:orden_id },
            success: function(d){
                location.reload();
            }
        });
    });
</script>
@stop
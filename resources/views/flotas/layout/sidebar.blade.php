<div class="sidebar" data-color="blue">
    <div class="logo">
        <a href="#" class="logo-text">Volkswagen</a>
    </div>
    <div class="logo logo-mini">
     <a href="#" class="logo-text">VW</a>
 </div>
 <div class="sidebar-wrapper">
    <div class="user">
        <div class="photo"><img src="{{asset('static-cotizador/img/default-avatar.png')}}" /></div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">Tania Andrew<b class="caret"></b></a>
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <li><a href="#">My Profile</a></li>
                    <li><a href="#">Edit Profile</a></li>
                    <li><a href="#">Settings</a></li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="nav">
        <li class="active">
            <a href="{{route('flotas.dashboard.main')}}"><i class="pe-7s-graph"></i><p>Dashboard</p></a>
        </li>
        <li>
            <a href="{{route('flotas.dashboard.ordenes')}}"><i class="pe-7s-plugin"></i><p>Órdenes</p></a>
        </li>
        <li>
            <a href="{{route('flotas.dashboard.clientes')}}"><i class="pe-7s-plugin"></i><p>Clientes</p></a>
        </li>
    </ul>
</div>
</div>
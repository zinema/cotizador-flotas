<?php

namespace App\Http\Controllers\Flotas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;

class ComentarioController extends Controller
{

    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->user_id = $request->user_id;
        $comment->comentario = $request->comentario;
        $comment->orden_id = $request->orden_id;
        $comment->save();
    }
}

<?php

namespace App\Http\Controllers\Flotas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use PDF;
use App\Orden;
use App\Vehicle;
use App\Cliente;
use App\Empresa;
use App\Aditional;
use App\Version;
use App\Modelo;
use Carbon\Carbon;


class OrdenController extends Controller
{
    public function index(){

    	$modelos = Modelo::orderBy('nombre')->get();

    	// $version = Version::orderBy('nombre')->get();

    	return view('flotas.dashboard.orden', compact('modelos'));
    }

    public function storeOrdenVendedor(Request $request){
    	if($request->filled(['nombres','apellidos','email','telefono'])){
			$id = DB::table('ordenes')->insertGetId(
			   	[
                 'user_id'            => $request->user_id,
			     'vendedor_nombres'   => $request->nombres, 
			     'vendedor_apellidos' => $request->apellidos,
			     'vendedor_email'	  => $request->email,
			     'vendedor_telefono'  => $request->telefono,
			     'created_at'         => Carbon::now()
			    ]
			);    		

			return json_encode($id);
    	}
    }

    public function storeOrdenEmpresa(Request $request){
		DB::table('ordenes')
		->where('id', $request->orden_id)
		->update(
		   	[
		     'empresa_razon_social' => $request->razon_social_empresa, 
		     'empresa_direccion' 	=> $request->direccion_empresa,
		     'empresa_ruc'	  		=> $request->ruc_empresa,
		     'empresa_telefono'	  	=> $request->telefono_empresa,
		    ]
		);    		

		if($request->filled(['orden_id', 'razon_social_empresa', 'direccion_empresa', 'ruc_empresa', 'telefono_empresa'])){
			$empresa = new Empresa;
			$empresa->razon_social	=	$request->razon_social_empresa;
			$empresa->ruc   		=	$request->ruc_empresa;
			$empresa->telefono 		=	$request->telefono_empresa;
			$empresa->direccion  	=	$request->direccion_empresa;
			$empresa->orden_id  	=	$request->orden_id;
			$empresa->save();
		}

    }

    public function storeOrdenCliente(Request $request){
		DB::table('ordenes')
		->where('id', $request->orden_id)
		->update(
		   	[
		     'contacto_nombres'  	 => $request->nombres_cliente, 
		     'contacto_apellidos' 	 => $request->apellidos_cliente,
		     'contacto_email'	 	 => $request->email,
		     'dni_cliente'	  	  	 => $request->dni,
		     'contacto_celular'   	 => $request->telefono,
			 'contacto_fecha_compra' =>	$request->fecha_compra_cliente,
		     'contacto_fuente'    	 => $request->fuente_contacto_cliente,
		    ]
		);    		

		if($request->filled(['orden_id', 'dni', 'nombres_cliente', 'apellidos_cliente', 'email', 'telefono', 'fuente_contacto_cliente']))
		{
			$cliente = new Cliente;
			$cliente->dni 					=	$request->dni;
			$cliente->nombres 				=	$request->nombres_cliente;
			$cliente->apellidos 			=	$request->apellidos_cliente;
			$cliente->telefono 				=	$request->telefono;
			$cliente->email 				=	$request->email;
			$cliente->fuente_contacto 		=	$request->fuente_contacto_cliente;
			$cliente->orden_id 				=	$request->orden_id;
			$cliente->save();
		}

    }

    public function storeOrdenVehiculo(Request $request){

//    	DB::table('ordenes')
//		->where('id', $request->user_id)
//		->update(
//		   	[
//		     'modelo_id'   	  => intval($request->modelo_1),
//		     'version_id'  	  => intval($request->version_1),
//		     'cantidad'	   	  => intval($request->cantidad),
//		     'color'   	   	  => $request->color,
//		     'fabricacion'    => $request->fabricacion,
//		     'anio'    		  => $request->year_modelo,
//		     'combustible'    => $request->combustible,
//		    ]
//		);

        $vehiculo = new Vehicle;
        $vehiculo->modelo_id    = $request->modelo_1;
        $vehiculo->version_id   = $request->version_1;
        $vehiculo->cantidad     = $request->cantidad_1;
        $vehiculo->color        = $request->color_1;
        $vehiculo->fabricacion  = $request->fabricacion_1;
        $vehiculo->year         = $request->year_modelo_1;
        $vehiculo->combustible  = $request->combustible_1;
        $vehiculo->orden_id     =  $request->orden_id;
        $vehiculo->save();

        $vehiculo = new Vehicle;
        $vehiculo->modelo_id    = $request->modelo_2;
        $vehiculo->version_id   = $request->version_2;
        $vehiculo->cantidad     = $request->cantidad_2;
        $vehiculo->color        = $request->color_2;
        $vehiculo->fabricacion  = $request->fabricacion_2;
        $vehiculo->year         = $request->year_modelo_2;
        $vehiculo->combustible  = $request->combustible_2;
        $vehiculo->orden_id     =  $request->orden_id;
        $vehiculo->save();

        $vehiculo = new Vehicle;
        $vehiculo->modelo_id    = $request->modelo_3;
        $vehiculo->version_id   = $request->version_3;
        $vehiculo->cantidad     = $request->cantidad_3;
        $vehiculo->color        = $request->color_3;
        $vehiculo->fabricacion  = $request->fabricacion_3;
        $vehiculo->year         = $request->year_modelo_3;
        $vehiculo->combustible  = $request->combustible_3;
        $vehiculo->orden_id     =  $request->orden_id;
        $vehiculo->save();

        $vehiculo = new Vehicle;
        $vehiculo->modelo_id    = $request->modelo_4;
        $vehiculo->version_id   = $request->version_4;
        $vehiculo->cantidad     = $request->cantidad_4;
        $vehiculo->color        = $request->color_4;
        $vehiculo->fabricacion  = $request->fabricacion_4;
        $vehiculo->year         = $request->year_modelo_4;
        $vehiculo->combustible  = $request->combustible_4;
        $vehiculo->orden_id     =  $request->orden_id;
        $vehiculo->save();



    }


    public function storeOrdenAdicional(Request $request){

    	if($request->filled('orden_id')){
	    	foreach ($request->adicional_nombre as $k => $val) {
		     	$adicional 			 = new Aditional;
		    	$adicional->producto = $request->adicional_nombre[$k];
		    	$adicional->precio   = $request->adicional_precio[$k];
		    	$adicional->orden_id = $request->orden_id;
		    	$adicional->save();
	    	}    		
    	}

    }

    public function downloadPdf(Request $request){
    	if($request->has('orden_id')){
	    	$orden = Orden::find($request->orden_id);
			$pdf = PDF::loadView('pdf.cotizador', compact('orden'));

			$name_file = 'cotizacion_'.time().'_'.$orden->id.'.pdf';
			$orden->pdf = $name_file;
			$orden->save();

			$pdf->save(public_path().'/'.$name_file);

			return $pdf->download($name_file);    		
    	}
    }

}

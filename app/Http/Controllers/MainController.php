<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class MainController extends Controller
{

  public function home(Request $request)
  {

  	if (Auth::check()) {
  		return redirect()->route('extranet.dashboard.main');
  	}
  	return redirect()->route('login');
  	
  }

}